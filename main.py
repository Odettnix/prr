import os
import math

clear = lambda: os.system('cls')
rez = 0

def kalc(a, b, num):
    try:
        if (num == 1):
            rez = a + b
            print("Ответ: " + str(a + b))
        if (num == 2):
            print("Ответ: " + str(a - b))
        if (num == 3):
            print("Ответ: " + str(a * b))
        if (num == 4):
            print("Ответ: " + str(a / b))
        if (num == 5):
            print("Ответ: " + str(math.ceil(a/b)))
        if (num == 7):
            print("Выберите число: "
                  "\n 1. " + str(a) + "\n 2. " + str(b))
            o = int(input())
            if (o == 1):
                print("Ответ: " + str(math.log10(a)))
            else:
                print("Ответ: " + str(math.log10(b)))
        if (num == 8):
            print("Выберите число: "
                  "\n 1. " + str(a) + "\n 2. " + str(b))
            o = int(input())
            if (o == 1):
                print("Ответ: " + str(math.log2(a)))
            else:
                print("Ответ: " + str(math.log2(b)))
        if (num == 9):
            print("Выберите число: "
                  "\n 1. " + str(a) + "\n 2. " + str(b))
            o = int(input())
            if (o == 1):
                print("Ответ: " + str(math.sqrt(a)))
            else:
                print("Ответ: " + str(math.sqrt(b)))
        if (num == 6):
            print("Ответ: " + str(math.fmod(a, b)))
        if (num == 10):
            print("Введите ещё одну переменную для подсчёта площади: ")
            c = float(input())
            Area = {
                'P': (lambda a, b: 2*(a + b)),
                'S': (lambda a, b, c: 2*(a * b + b * c + a * c)),
            }
            print('Периметр = ', Area['P'](a, b))
            print('Площадь = ', Area['S'](a, b, c))
        if (num == 11):
            if(a < 0):
                print("\n Для корректности вычислений числа были возведены в модуль\n")
            if(b < 0):
                print("\n Для корректности вычислений числа были возведены в модуль\n")
            proc = b * 100 / a
            print("Ответ: " + str(math.fabs(proc)) + " %")
    except:
        print("Данное число не совсем подходят")

def stroka(s):
    clear()
    print("Строка - " + str(s))
    print("Длина строки: " + str(len(s)))
    print("Пробелы: " + str(s.count(' ')))
    print("Запятые: " + str(s.count(',')))



while True:
    print("\n=========================\nВыберите нужный вариант:\n========================="
          "\n Функция 1. Калькулятор"
          "\n Функция 2. Подсчет в строке"
          "\n Функция 3. Матрица"
          "\n 0. Выход")
    try:
        variant = int(input())
        if (variant == 1):
            while True:
                print("Введите числа: ")
                a = float(input())
                b = float(input())
                while True:
                    print("\n===================\nВыберите действие:\n==================="
                          "\n 1. Сложение"
                          "\n 2. Вычитание"
                          "\n 3. Умножение"
                          "\n 4. Деление"
                          "\n 5. Деление нацело"
                          "\n 6. Остаток"
                          "\n 7. Логарифм по основанию 10"
                          "\n 8. Логарифм по основанию 2"
                          "\n 9. Корень"
                          "\n 10. Площадь и периметр параллелепипеда"
                          "\n 11. Процент от числа a"
                          "\n 0. Назад")
                    num = int(input())
                    kalc(a, b, num)
                    if (num == 0):
                        clear()
                        break
                if (num == 0):
                    clear()
                    break

        elif (variant == 0):
            print("пока-пока")
            break
        elif (variant == 2):
            print("Ввеедите слово или предложение: ")
            s = str(input())
            if(s != ""):
                stroka(s)
        elif (variant == 3):
            try:
                print("Количество столбцов: ")
                N = int(input())
                print("Количество строк: ")
                M = int(input())
                A = []
                for i in range(N):
                    A.append([0] * M)
                print("Число, с которого начинается массив: ")
                g = int(input())
                print("Увеличитель: ")
                c = int(input())
                for i in range(N):
                    for j in range(M):
                        A[i][j] = g
                        g += c

                for i in range(len(A)):
                    for j in range(len(A[i])):
                        print(A[i][j], end=' ')
                    print()
                print("Столбцы: " + str(N) + " \nСтроки: " + str(M) + "\n")
            except:
                print("а ой")

    except:
        clear()
        print("Что-то не так")
